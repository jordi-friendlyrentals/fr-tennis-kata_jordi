﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tennis_kataTest
{
    [TestClass]
    public class TennisScoreTest
    {
        private TennisScore Score { get; set; }

        [TestInitialize]
        public void TestInit()
        {
            Score = new TennisScore("Chile", "Venezuelas");
        }
        
        [TestMethod]
        public void GetScore_WhenPointsUntilTwo_ThenScoreIsCorrect()
        {
            Assert.AreEqual("love-love", Score.GetScore(0, 0));
            Assert.AreEqual("love-fifteen", Score.GetScore(0, 1));
            Assert.AreEqual("fifteen-love", Score.GetScore(1, 0));
            Assert.AreEqual("fifteen-fifteen", Score.GetScore(1, 1));
            Assert.AreEqual("love-thirty", Score.GetScore(0, 2));

            Assert.AreEqual("fifteen-thirty", Score.GetScore(1, 2));
            Assert.AreEqual("thirty-thirty", Score.GetScore(2, 2));
            Assert.AreEqual("thirty-love", Score.GetScore(2, 0));
            Assert.AreEqual("thirty-fifteen", Score.GetScore(2, 1));
        }

        [TestMethod]
        public void GetScore_WhenPointsUntilThree_ThenScoreIsCorrect()
        {
            Assert.AreEqual("fifteen-forty", Score.GetScore(1, 3));
        }

        [TestMethod]
        public void GetScore_WhenPointsArriveFourAndDiffMoreThanTwo_ThenScoreGetsTheWinner()
        {
            Assert.AreEqual("player2 wins", Score.GetScore(2, 4));
        }

        [TestMethod]
        public void GetScore_WhenPointsAreThreeOnBoth_ThenScoreIsDeuce()
        {
            Assert.AreEqual("deuce", Score.GetScore(3, 3));
        }

        [TestMethod]
        public void GetScore_WhenPointsAreThreeOrMoreOnBoth_ThenScoreIsDeuce()
        {
            Assert.AreEqual("deuce", Score.GetScore(4, 4));
            Assert.AreEqual("deuce", Score.GetScore(7, 7));
        }

        [TestMethod]
        public void GetScore_WhenPointsArriveFourAndDiffOnlyOne_ThenScoreGetsAdvantageWithName()
        {
            Assert.AreEqual("advantage Venezuelas", Score.GetScore(3, 4));
        }

    }
}
