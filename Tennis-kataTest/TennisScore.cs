﻿using System;

namespace Tennis_kataTest
{
    internal class TennisScore
    {
        private string _namePlayer1;
        private string _namePlayer2;

        public TennisScore()
        {
        }

        public TennisScore(string player1, string player2)
        {
            this._namePlayer1 = player1;
            this._namePlayer2 = player2;
        }

        internal object GetScore(int pointsPlayer1, int pointsPlayer2)
        {
            var specialMessage = GetSpecialMessage(pointsPlayer1, pointsPlayer2);
            if (!string.IsNullOrEmpty(specialMessage)) return specialMessage;

            var score1 = GetTennisScore(pointsPlayer1);
            var score2 = GetTennisScore(pointsPlayer2);
            
            return score1 + "-" + score2;
        }

        private string GetSpecialMessage(int pointsPlayer1, int pointsPlayer2)
        {
            if ((pointsPlayer1 > 3) || (pointsPlayer2 > 3))
            {
                if (pointsPlayer1 >= pointsPlayer2 + 2) return "player1 wins";
                if (pointsPlayer1 + 2 <= pointsPlayer2) return "player2 wins";

                if (pointsPlayer1 > pointsPlayer2) return "advantage " + this._namePlayer1;
                if (pointsPlayer1 < pointsPlayer2) return "advantage " + this._namePlayer2;
            }
            if ((pointsPlayer1 > 2) & (pointsPlayer2 > 2))
            {
                if (pointsPlayer1 == pointsPlayer2) return "deuce";
            }
            return null;
        }

        string GetTennisScore(int points)
        {
            var score = "love";
            switch (points)
            {
                case 1: score = "fifteen"; break;
                case 2: score = "thirty"; break;
                case 3: score = "forty"; break;
                default: break;
            }
            return score;
        }
    }
}